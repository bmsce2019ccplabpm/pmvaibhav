 #include<stdio.h>
 int input_hrs()
{
    int hrs;
    printf("Enter the time in hours\n");
    scanf("%d",&hrs);
    return hrs;
}
 int input_mins()
{
    int mins;
    printf("Enter the time in minutes\n");
    scanf("%d",&mins);
    return mins;
}
 int time(int h,int m)
{
    int c = (h*60)+m;
    return c;
}
 int display(int a)
{
    printf("The total time in hours = %d",a);
}
 int main()
{
    int x,y,tmin;
    x = input_hrs();
    y = input_mins();
    tmin = time(x,y);
    display(tmin);
    return 0;
}
